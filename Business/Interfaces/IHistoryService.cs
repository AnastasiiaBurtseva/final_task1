﻿using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
   public interface IHistoryService:IDisposable
    {
        /// <summary>
        /// /find history by id
        /// </summary>
        /// <param name="id1"></param>
        /// <returns></returns>
        HistoryModel FindHistory(int id1);
        /// <summary>
        /// create new history
        /// </summary>
        /// <param name="model"></param>
        void NewHistory(HistoryModel model);
        /// <summary>
        /// add messagr to history
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
         Task addMessage(MessageModel model);
        List<HistoryModel> GetAll();
        /// <summary>
        /// update list of messages
        /// </summary>
        /// <param name="m"></param>
        /// <param name="Idh"></param>
        /// <returns></returns>
        Task updatemessages(Message m, int Idh);
        ICollection<MessageModel> GetAllMessages(int IdHistory);
        /// <summary>
        ///  another way for find history
        /// </summary>
        /// <param name="senderId"></param>
        /// <param name="receiverId"></param>
        /// <returns></returns>
        HistoryModel FindHistory(string senderId, string receiverId);
        ICollection<HistoryModel> GettAllHistories(string user);
    }
}
