﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Business.Models;
using Business.OurInfrastructure;
using System.Collections.Generic;
using System.Security.Claims;
namespace Business.Interfaces
{
    public interface IUserService : IDisposable
    {
        int DeleteById(string id);
        Task<OperationDetails> Create(UserModel userDto);
        Task<ClaimsIdentity> Authenticate(UserModel userDto);
        Task SetInitialData(UserModel adminDto, List<string> roles);
        IUserService CreateUserService(string v);

        ICollection<UserModel> GetAllFriends(UserModel user);
        ICollection<UserModel> GetAllUsers();
        UserModel Get(string id);
        UserModel Getwithoutdetails(string id);
        UserModel GetUser(string v, string d);
        void AddFriend(string Home, string friend);
        void onlyUpdate(UserModel user);
        List<UserModel> FindUser(UserModel model);
    }
}
