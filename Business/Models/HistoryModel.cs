﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
   public class HistoryModel
    {
        public int Id { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        /// <summary>
        /// id's formessasges
        /// </summary>
        public ICollection<int> messagesIds { get; set; }
    }
}
