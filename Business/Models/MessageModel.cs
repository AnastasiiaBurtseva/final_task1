﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
   public  class MessageModel
    {
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }
        public DateTime time { get; set; }

        public int idHistory { get; set; }
        public string senderNick { get; set; }
    }
}
