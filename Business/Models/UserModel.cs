﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
   public class UserModel
    {
        public string Id { get; set; }

        public string name { get; set; }
        [Required]
        public string nick { get; set; }
        [Required]
        public string password { get; set; }
        /// <summary>
        /// id's forfriends
        /// </summary>
        public ICollection<string> FriendsIds { get; set; }
     
        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobile { get; set; }
        
        public DateTime date { get; set; }
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]

        public string email { get; set; }
        public string role { get; set; }
    }
}
