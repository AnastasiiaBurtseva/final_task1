﻿using Data.Context;
using Data.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.OurInfrastructure
{
   public class LogikModul : NinjectModule
    {
        private string connectionString;
        public LogikModul(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitofWork>().To<UnitofWork>().WithConstructorArgument(connectionString);

        }


    }
}
