﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Context;
using Business.Models;
using Data.Entities;
using AutoMapper;
using Data.Interfaces;
using Business.Interfaces;

namespace Business.Services
{
   public class HistoryServise:IHistoryService
    {
        IUnitofWork unitOfWork;
        IMapper mapper;
        public HistoryServise(IUnitofWork now)
        {
            unitOfWork = now;
             mapper = new MapperConfiguration(cfg => cfg.CreateMap<History, HistoryModel>()
     .ForMember(p => p.messagesIds, c => c.MapFrom(x => x.messages.Select(t => t.Id)))
     .ForMember(d => d.receiverId, c => c.MapFrom(x => x.receiver.Id))
     .ForMember(p => p.senderId, c => c.MapFrom(x => x.sender.Id))).CreateMapper();
        }
        /// <summary>
        /// /find history
        /// </summary>
        /// <param name="id1"></param>
        /// <returns></returns>
        public HistoryModel FindHistory(int id1)
        {
           
            return mapper.Map<History, HistoryModel>( unitOfWork.HistoryRepository.GetById1(id1));

        }
        public void NewHistory(HistoryModel model)
        {
            
             var sender = unitOfWork.UserRepository.GetById(model.senderId);
             var receiver = unitOfWork.UserRepository.GetById(model.receiverId);
            History history = new History
            {
                Id = model.Id,
                sender = sender,
                receiver = receiver,
                senderId = model.senderId,
                receiverId = model.receiverId,
                messages = new List<Message>()


            };
            unitOfWork.HistoryRepository.Add(history);
            unitOfWork.SaveAsync();
              


        }
        /// <summary>
        /// add message to MessageRepository
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async  Task addMessage(MessageModel model)
        {
            Message m = new Message
            {
                Id = model.Id,
                Text = model.Text,
                time = model.time,
                idHistory=model.idHistory,
                senderNick=model.senderNick
                
            };
            unitOfWork.MessageRepository.Add(m);
           

           await updatemessages(m,model.idHistory);
           

        }
        public List<HistoryModel> GetAll()
        {
         
            return mapper.Map<IEnumerable<History>, List<HistoryModel>>(unitOfWork.HistoryRepository.FindAll());//temp;
        }
        /// <summary>
        /// этот не зывается с контроллера
        /// </summary>
        /// <param name="m"></param>
        /// <param name="Idh"></param>
        /// <returns></returns>
        public async Task updatemessages(Message m,int Idh) 
        {
            var newHistory = await unitOfWork.HistoryRepository.GetByIdAsync(Idh);
            newHistory.messages.Add(m);

            unitOfWork.HistoryRepository.Update(newHistory);
          await  unitOfWork.SaveAsync();
        }

        /// <summary>
        /// all messages by id
        /// </summary>
        /// <param name="IdHistory"></param>
        /// <returns></returns>
        public ICollection<MessageModel> GetAllMessages(int IdHistory)
        {
           
            var mapper2 = new MapperConfiguration(cfg => cfg.CreateMap<Message, MessageModel>()).CreateMapper();

           // var temp = GetAll();
            var temp2 = mapper2.Map<IEnumerable<Message>, List<MessageModel>>(unitOfWork.MessageRepository.FindAll());
            var ourMessages = temp2.FindAll(x => x.idHistory == IdHistory);
            return ourMessages;
        }
        public ICollection<HistoryModel>GettAllHistories(string user)
        {
            return GetAll().ToList().FindAll(x => x.receiverId== user || x.senderId == user);
        }
        public HistoryModel FindHistory(string senderId,string receiverId)
        {
            var list = GetAll();
          return  list.Find(x => x.senderId == senderId && x.receiverId == receiverId);

        }


        public void Dispose()
        {
          
            throw new NotImplementedException();
        }
    }
}
