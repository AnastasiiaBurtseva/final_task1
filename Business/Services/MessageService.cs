﻿using Business.Interfaces;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class MessageService : IMessageService
    {

        IUnitofWork unitOfWork;
        
        public MessageService(IUnitofWork now)
        {
            unitOfWork = now;
           

        }
        public void DeleteMessage(int id)
        {
            unitOfWork.MessageRepository.DeleteById(id);
        }
    }
}
