﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Context;
using Business.Models;
using Data.Entities;
using AutoMapper;
using Data.Interfaces;
using Business.Interfaces;

namespace Business.Services
{
  public  class RoleService : IRoleService
    {
        IUnitofWork unitOfWork;
        IMapper mapper;
        public RoleService(IUnitofWork now)
        {
            unitOfWork = now;
            mapper = new MapperConfiguration(cfg => cfg.CreateMap<History, HistoryModel>()
    .ForMember(p => p.messagesIds, c => c.MapFrom(x => x.messages.Select(t => t.Id)))
    .ForMember(d => d.receiverId, c => c.MapFrom(x => x.receiver.Id))
    .ForMember(p => p.senderId, c => c.MapFrom(x => x.sender.Id))).CreateMapper();

        }
    

       
     
        public IEnumerable<HistoryModel> GetHistory(string id)
        {
           
            var temp = mapper.Map<IEnumerable<History>, List<HistoryModel>>(unitOfWork.HistoryRepository.FindAll());
            return temp.ToList().FindAll(x=>x.receiverId==id || x.senderId==id);
        }
        public IEnumerable<MessageModel> GetMessages(int id1)
        {

         
            var mapper2 = new MapperConfiguration(cfg => cfg.CreateMap<Message, MessageModel>()).CreateMapper();

            var temp = mapper.Map<IEnumerable<History>, List<HistoryModel>>(unitOfWork.HistoryRepository.FindAll());
            var temp2 = mapper2.Map<IEnumerable<Message>, List<MessageModel>>(unitOfWork.MessageRepository.FindAll());
            var ourMessages = temp2.FindAll(x => x.idHistory == id1);
            return ourMessages;
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
         
        }
    }
}
