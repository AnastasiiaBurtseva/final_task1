﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Context;
using Business.Models;
using Data.Entities;
using AutoMapper;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using Business.Interfaces;
using Business.OurInfrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Data.Interfaces;

namespace Business.Services
{
    public class UserServices:IUserService
    {
        IUnitofWork unitOfWork;
      
        public UserServices(IUnitofWork now)
        {
            unitOfWork = now;
        
        }
        /// <summary>
        /// delete by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteById(string id)
        {
           if( unitOfWork.HistoryRepository.FindAll().ToList().FindAll(x=>x.receiverId==id || x.senderId == id).Count() !=0){
                return 1;
            }
            unitOfWork.UserRepository.DeleteById(id);
            return 0;
        }
       
        public ICollection<UserModel> GetAllUsers()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OurUser, UserModel>()
            .ForMember(p => p.FriendsIds, c => c.MapFrom(x => x.Friends.Select(t => t.Id)) )).CreateMapper();
          
            var temp = mapper.Map<List<UserModel>>(unitOfWork.UserRepository.GetAllWithDetails());
            return temp;
        }
        /// <summary>
        /// get friends
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ICollection<UserModel> GetAllFriends(UserModel model)
        {
            
            var temp = GetAllUsers().ToList();
            var ourUser = temp.Find(x => x.Id == model.Id);
            List<UserModel> friends = new List<UserModel>();
            foreach (var i in ourUser.FriendsIds)
            {
                friends.Add(temp.Find(x => x.Id == i));
            }
            return friends;
        }
      

        public UserModel Get(string id)
        {
             var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OurUser, UserModel>()
            .ForMember(p => p.FriendsIds, c => c.MapFrom(x => x.Friends.Select(t => t.Id)))).CreateMapper();
            
            return mapper.Map<UserModel>(unitOfWork.UserRepository.GetByIdWithDetails(id));
        }

        /// <summary>
        /// get withot navigation properties
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserModel Getwithoutdetails(string id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OurUser, UserModel>()
           .ForMember(p => p.FriendsIds, c => c.MapFrom(x => x.Friends.Select(t => t.Id)))).CreateMapper();
          

            return mapper.Map<UserModel>(unitOfWork.UserRepository.GetById(id));//temp.Find(x=>x.Id==id);
        }
      
        public UserModel GetUser(string nick,string password)
        {


          
            var allUser = GetAllUsers().ToList();
            
            return allUser.Find(x=>x.nick==nick && x.password==password);
        }
        /// <summary>
        /// add friends
        /// </summary>
        /// <param name="Home"></param>
        /// <param name="friend"></param>
        public void AddFriend(string Home,string friend)
        {
          
            var temp =  unitOfWork.UserRepository.GetAllWithDetails().ToList();
             
           var firstuser= temp.Find(x => x.Id == Home);
            var seconduser= temp.Find(x => x.Id == friend);
          

            firstuser.Friends.Add(temp.Find(x => x.Id == friend));
            seconduser.Friends.Add(temp.Find(x => x.Id == Home));
         


            unitOfWork.UserRepository.Update(firstuser);
            unitOfWork.UserRepository.Update(seconduser);
            unitOfWork.SaveAsync();
          
        }
        public void onlyUpdate(UserModel user)
        {
            var temp = unitOfWork.UserRepository.GetAllWithDetails().ToList();

            var newuser = temp.Find(x => x.Id == user.Id);
            
          
            newuser.mobile = user.mobile;
            newuser.email = user.email;
        
            unitOfWork.UserRepository.Update(newuser);
            unitOfWork.SaveAsync();
        }
        public List<UserModel> FindUser(UserModel model)
        {
           
            var users = GetAllUsers().ToList();
            if (model.email != null)
            {
             
                return users.FindAll(p => p.email== model.email);

            }
            else if (model.mobile != null)
            {
              
                return users.FindAll(p => p.mobile == model.mobile);
            }
            else if (model.nick != null)
            {
               
                return users.FindAll(p => p.nick == model.nick);
            }
            else
                return new List<UserModel>();
        }

        public async Task<OperationDetails> Create(UserModel userDto)
        {
            ApplicationUser user = await unitOfWork.UserManager.FindByEmailAsync(userDto.nick);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDto.nick, UserName = userDto.name};
                var result = await unitOfWork.UserManager.CreateAsync(user, userDto.password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
               
                await unitOfWork.UserManager.AddToRoleAsync(user.Id, "user");
              
                OurUser clientProfile = new OurUser{ Id = user.Id,name=userDto.name,nick=userDto.nick,password=userDto.password,role=userDto.role };
                unitOfWork.UserRepository.Add(clientProfile);
                await unitOfWork.SaveAsync();
                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
          
        }

        public async Task<ClaimsIdentity> Authenticate(UserModel userDto)
        {
            ClaimsIdentity claim = null;
          
            ApplicationUser user = await unitOfWork.UserManager.FindAsync(userDto.nick, userDto.password);
          
            if (user != null)
                claim = await unitOfWork.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
            
        }
       
        public async Task SetInitialData(UserModel adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await unitOfWork.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await unitOfWork.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
           
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
           
        }

        public IUserService CreateUserService(string v)
        {
            throw new NotImplementedException();
        }
    }
}
