﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Data.Identity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.Context
{
  public  class OurContext: IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// data context declaration
        /// </summary>
        public DbSet<OurUser> profiles { get; set; }
        public DbSet<Message> messages { get; set; }
        public DbSet<History> histories { get; set; }
         
        public OurContext(string connectionString)
            : base(connectionString)
        { }
        public OurContext()
           
        { }
       public class DbInitializer :DropCreateDatabaseAlways<OurContext>
        { }

        
    }
}
