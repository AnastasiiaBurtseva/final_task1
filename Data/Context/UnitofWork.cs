﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Repositories;
using Data.Identity;
using Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.Context
{
  

    public class UnitofWork : IUnitofWork
    {
        private readonly OurContext db;
        private UserRepository userRepository;
        private HistoryRepository historyRepository;
        private MessageRepository messageRepository;
      
        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
        private ClientManager clientManager;
       
        public UnitofWork(string connectionString)
        {
            db = new OurContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new ClientManager(db);
           
        }
        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }
        public ClientManager ClientManager
        {
            get { return clientManager; }
        }


        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                }
                this.disposed = true;
            }
        }
    
    public IUserRepository UserRepository {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }
        }

        public IHistoryRepository HistoryRepository
        {
            get
            {
                if (historyRepository == null)
                    historyRepository = new HistoryRepository(db);
                return historyRepository;
            }
        }

        public IMessageRepository MessageRepository
        {
            get
            {
                if (messageRepository == null)
                    messageRepository = new MessageRepository(db);
                return messageRepository;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }

    }
}
