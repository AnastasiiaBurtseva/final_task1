﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
namespace Data.Entities
{
   public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }
        /// <summary>
        /// cllient profile for sharing user data
        /// </summary>
        public virtual OurUser ClientProfile { get; set; }
    }
}
