﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
  public class History:BaseEntity
    {
        public int Id { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        /// <summary>
        /// our sender
        /// </summary>
        public OurUser sender { get; set; } 
        /// <summary>
        /// our receiver
        /// </summary>
        public OurUser receiver { get; set; }

        public ICollection<Message> messages { get; set; }
    }
} 
