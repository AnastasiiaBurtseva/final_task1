﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
  public class Message:BaseEntity
    {

        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime time { get; set; }
         public int idHistory { get; set; }

        public string senderNick { get; set; }
      
    }
}
