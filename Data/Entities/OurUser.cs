﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Data.Entities
{
  public class OurUser:BaseEntity
    {
        /// <summary>
        /// one to one relationship
        /// </summary>
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        public string name { get; set; }
        /// <summary>
        /// login for our user
        /// </summary>
        public string nick { get; set; }

        public string password { get; set; }
        /// <summary>
        /// List of friends
        /// </summary>

        public ICollection <OurUser> Friends { get; set; }

     
        public string mobile { get; set; }
       
        public string email { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public string role { get; set; }



    }
}
