﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.Identity
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        /// <summary>
        /// role management
        /// </summary>
        /// <param name="store"></param>
        public ApplicationRoleManager(RoleStore<ApplicationRole> store)
                    : base(store)
        { }
    }
}
