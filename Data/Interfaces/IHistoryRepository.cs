﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;


namespace Data.Interfaces
{
    public interface IHistoryRepository:IRepository<History>
    {
        /// <summary>
        /// async by int
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
         Task<History> GetByIdAsync(int id);
        /// <summary>
        /// by int
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        History GetById1(int id);
    }
}
