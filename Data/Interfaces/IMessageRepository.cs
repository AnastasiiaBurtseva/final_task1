﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;

namespace Data.Interfaces
{
   public interface IMessageRepository : IRepository<Message>
    {
        /// <summary>
        /// int ID
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(int id);
    }
}
