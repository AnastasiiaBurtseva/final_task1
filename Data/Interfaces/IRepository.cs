﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
   public interface IRepository<TEntity> where TEntity : BaseEntity
    {
    IQueryable<TEntity> FindAll();

    TEntity GetById(string id);

    void Add(TEntity entity);

    void Update(TEntity entity);

    void Delete(TEntity entity);

    void DeleteById(string id);
}

    
}
