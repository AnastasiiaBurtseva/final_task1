﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Identity;
using System;
using System.Threading.Tasks;
namespace Data.Interfaces
{
  public  interface IUnitofWork: IDisposable
    {
        IUserRepository UserRepository { get; }

        IHistoryRepository HistoryRepository { get; }

        IMessageRepository MessageRepository { get; }

        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}
