﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Histories", new[] { "receiver_Id" });
            DropIndex("dbo.Histories", new[] { "sender_Id" });
            DropColumn("dbo.Histories", "receiverId");
            DropColumn("dbo.Histories", "senderId");
            RenameColumn(table: "dbo.Histories", name: "receiver_Id", newName: "receiverId");
            RenameColumn(table: "dbo.Histories", name: "sender_Id", newName: "senderId");
            AlterColumn("dbo.Histories", "senderId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Histories", "receiverId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Histories", "senderId");
            CreateIndex("dbo.Histories", "receiverId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Histories", new[] { "receiverId" });
            DropIndex("dbo.Histories", new[] { "senderId" });
            AlterColumn("dbo.Histories", "receiverId", c => c.Int());
            AlterColumn("dbo.Histories", "senderId", c => c.Int());
            RenameColumn(table: "dbo.Histories", name: "senderId", newName: "sender_Id");
            RenameColumn(table: "dbo.Histories", name: "receiverId", newName: "receiver_Id");
            AddColumn("dbo.Histories", "senderId", c => c.Int());
            AddColumn("dbo.Histories", "receiverId", c => c.Int());
            CreateIndex("dbo.Histories", "sender_Id");
            CreateIndex("dbo.Histories", "receiver_Id");
        }
    }
}
