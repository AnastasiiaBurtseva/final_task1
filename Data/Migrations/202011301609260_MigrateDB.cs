﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OurUsers", "date", c => c.DateTime(nullable: false));
            AddColumn("dbo.OurUsers", "email", c => c.String());
            DropColumn("dbo.OurUsers", "data");
            DropColumn("dbo.OurUsers", "country");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OurUsers", "country", c => c.String());
            AddColumn("dbo.OurUsers", "data", c => c.String());
            DropColumn("dbo.OurUsers", "email");
            DropColumn("dbo.OurUsers", "date");
        }
    }
}
