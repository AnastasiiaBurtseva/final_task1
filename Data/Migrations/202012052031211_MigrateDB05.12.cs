﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateDB0512 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "senderNick", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "senderNick");
        }
    }
}
