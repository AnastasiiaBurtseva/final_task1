﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateDB0712 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.OurUsers", "date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OurUsers", "date", c => c.DateTime(nullable: false));
        }
    }
}
