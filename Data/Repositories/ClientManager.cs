﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ClientManager : IClientManager
    {
        public OurContext Database { get; set; }
        public ClientManager(OurContext db)
        {
            Database = db;
        }

        public void Create(OurUser item)
        {
            Database.profiles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
