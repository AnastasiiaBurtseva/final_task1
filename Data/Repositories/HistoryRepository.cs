﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using Data.Context;
using System.Data.Entity;

namespace Data.Repositories
{
    public class HistoryRepository : IHistoryRepository
    {
        private OurContext db;
        public HistoryRepository(OurContext context)
        {
            this.db = context;
        }
        public void Add(History entity)
        {
            db.histories.Add(entity);
           
        }

        public void Delete(History entity)
        {
            History history = db.histories.Find(entity.Id);
            if (history != null)
                db.histories.Remove(history);
        }

        public void DeleteById(int id)
        {
            History history = db.histories.Find(id);
            if (history != null)
                db.histories.Remove(history);
            db.SaveChanges();
        }

        public void DeleteById(string id)
        {
            throw new NotImplementedException();
        }

        public  IQueryable<History> FindAll()
        {
            return  db.histories.Include(x => x.messages).Include(x => x.receiver).Include(x => x.sender);
               
           
        }

       

        public async Task< History> GetByIdAsync(int id)
        {
            var p = await db.histories.FindAsync(id);
            db.Entry(p).Collection("messages").Load();

            return p;

        }
      
        public void Update(History entity)
        {
            db.Entry(entity).State = EntityState.Modified;
          
        }

        public History GetById1(int id)
        {
            var p = db.histories.Find(id);
            db.Entry(p).Collection("messages").Load();

            return p;

           
        }

        public History GetById(string id)
        {
            throw new NotImplementedException();
        }
    }
}
