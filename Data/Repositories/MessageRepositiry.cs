﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Data.Context;
using System.Data.Entity;

namespace Data.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private OurContext db;
        public MessageRepository(OurContext context)
        {
            this.db = context;
        }
        public void Add(Message entity)
        {
            db.messages.Add(entity);
            db.SaveChanges();
          
        }

        public void Delete(Message entity)
        {
            db.messages.Remove(entity);
            db.SaveChanges();
        }

        public void DeleteById(int id)
        {
            Message m = db.messages.Find(id);
            if (m != null)
                db.messages.Remove(m);
            db.SaveChanges();
        }

        public void DeleteById(string id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Message> FindAll()
        {
            return db.messages;
        }

        public Message GetById(int id)
        {
            var p = db.messages.Find(id);
            return p;
        }

        public Message GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Message entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
