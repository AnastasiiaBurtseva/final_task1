﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Data.Context;
using System.Security.Cryptography.X509Certificates;
using System.Data.Entity;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// our context
        /// </summary>
        private OurContext db { get; set; }
        public UserRepository(OurContext context)
        {
            this.db = context;
        }
        public void Create(OurUser item)
        {
            db.profiles.Add(item);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
        public void Add(OurUser entity)
        {
           db.profiles.Add(entity);
          
        }

        public void Delete(OurUser entity)
        {
            OurUser user = db.profiles.Find(entity.Id);
            if (user != null)
                db.profiles.Remove(user);

            
        }

        public void DeleteById(string id)
        {
            OurUser user = db.profiles.Find(id);
            if (user != null)
                db.profiles.Remove(user);
            db.SaveChanges();
            
        }

        public IQueryable<OurUser> FindAll()
        {
           return db.profiles;
          
        }

        public IQueryable<OurUser> GetAllWithDetails()
        {
            return db.profiles.Include
              (p => p.Friends)
              ;
          
        }

        public OurUser GetById(string id)
        {
            var p = db.profiles.Find(id);
            return p;
            
        }

        public OurUser GetByIdWithDetails(string id)
        {
            
            var p =  db.profiles.Find(id);
            db.Entry(p).Collection("Friends").Load();

            return p;
        }

        public void Update(OurUser entity)
        {
            db.Entry(entity).State = EntityState.Modified;
           // db.SaveChanges();
            
        }

      /*  public void UpdateFriends(OurUser newFriend)//?
        {
            throw new NotImplementedException();
        }*/

       
    }
}
