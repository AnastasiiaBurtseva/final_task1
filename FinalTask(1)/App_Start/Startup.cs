﻿
using AutoMapper;
using Business.Interfaces;
using Business.OurInfrastructure;
using Business.Services;


using FinalTask_1_.Util;
using Microsoft.AspNet.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
[assembly: OwinStartup(typeof(FinalTask_1_.App_Start.Startup))]
namespace FinalTask_1_.App_Start
{

    public class Startup
    {
        NinjectModule orderModule = new OrderModule();


        public void Configuration(IAppBuilder app)
            {
                app.CreatePerOwinContext<IUserService>(CreateUserService);
           // app.CreatePerOwinContext<IRoleService>(CreateRolesService);
            //   app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
                {
                    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                    LoginPath = new PathString("/Account/Login"),
                });
            }
        
        private IUserService CreateUserService()
            {
           // var kernel = new StandardKernel(orderModule);
           // DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            return  DependencyResolver.Current.GetService<UserServices>();

            //return logikModule.CreateUserService("DefaultConnection");
            }
        private IRoleService CreateRolesService()
        {
            // var kernel = new StandardKernel(orderModule);
            // DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            return DependencyResolver.Current.GetService<RoleService>();

            //return logikModule.CreateUserService("DefaultConnection");
        }
        // настраиваем контекст и менеджер

    }
}