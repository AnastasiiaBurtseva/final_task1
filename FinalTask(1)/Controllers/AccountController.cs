﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using FinalTask_1_.Models;
using Business.Models;
using System.Security.Claims;
using Business.Interfaces;
using Business.OurInfrastructure;
using Microsoft.AspNet.Identity;

namespace FinalTask_1_.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("Ключ", "Значение");

            return View(data);
        }

        IUserService userservice;
        public  AccountController(IUserService now)
        {
          
            userservice = now;
           
        }
        private IUserService UserService
        {
            get
            {
                return userservice;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// Enter to Personal Page
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginModel model)
        {
           

                UserModel userDto = new UserModel { nick = model.nick, password = model.password };
                ClaimsIdentity claim = await userservice.Authenticate(userDto);

                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                ViewBag.errors = "Неверный логин или пароль.";
                return View(model);
            }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                UserModel Model = userservice.GetUser(model.nick,model.password);
              
                return RedirectToAction("PersonalPage", "Home",new { id1 = Model.Id });
            }
           
           
        }
       
        
      

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
       
        public async Task<ActionResult> Register(Registrashion model)
        {
           
            if (ModelState.IsValid)
            {
                UserModel user = new UserModel
                {
                    nick = model.nick,
                    password = model.Password,

                    name = model.nick,
                    role = "user",
                    FriendsIds = new List<string>()
                    
                };
                OperationDetails result = await UserService.Create(user);
                if (result.Succedeed)
                {
                    return View("SuccessRegister");
                }
                else
                {
                    
                    ViewBag.errors = result.Message;
                    return View();
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewBag.errors = errors;
                return View();
            }
           
        }
    
        private  void SetInitialData()
        {
             userservice.SetInitialData(new UserModel
            {
                nick = "Some",
              
                password = "1111111",
                name = "Semen",
               
                role = "admin",
            }, new List<string> { "user", "admin" });
        }
    }
}
