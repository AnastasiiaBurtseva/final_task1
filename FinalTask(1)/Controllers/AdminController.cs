﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Interfaces;
using Business.Models;
using Business.Services;


namespace FinalTask_1_.Controllers
{ 
   public class AdminController : Controller
    {
      
        IUserService service;
        IHistoryService service2;
        IMessageService serviceM;
        public AdminController(IUserService now,IHistoryService now2,IMessageService now3)
        {
            service = now;
            service2 = now2;
            serviceM = now3;
        }

        [Authorize(Roles = "admin")]

        public ActionResult DeleteUser(string id1)
        {

            if (service.DeleteById(id1) == 1) {
                ViewBag.str = "У пользователя уже есть истории,его нельзя удалить";
                return View();
            }
            ViewBag.str = " Пользователь удален ";

            return View();

        }
        [Authorize(Roles = "admin")]

        public ActionResult WatchAllUsers()
        {

            ViewBag.users = service.GetAllUsers();
            return View();

        }
        [Authorize(Roles = "admin")]

        public ActionResult WatchHistory(string id1)
        {
            var history = service2.GettAllHistories(id1);
            List<UserModel> users = new List<UserModel>();
            if (history.Count() != 0)
            {
                foreach (var i in history)
                {
                    users.Add(service.Get(i.receiverId));
                }
            }
            ViewBag.user= users;
            ViewBag.histories = service2.GettAllHistories(id1);
            if (history == null)
            {
                ViewBag.error = "У этого пользователя нет переписок";
            }
            return View();
        }
        [Authorize(Roles = "admin")]

        public ActionResult GetMessages(int id1)
        {
            ViewBag.messages = service2.GetAllMessages(id1);
            return View();
        }
         public ActionResult DeleteMessage(int id1)
        {
            serviceM.DeleteMessage(id1);
            return View();
        }

    }
}