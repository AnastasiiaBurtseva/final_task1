﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalTask_1_.Models;
using Business.Models;
using Business.Services;
using System.Web.UI.WebControls;
using Business.Interfaces;
using System.Threading.Tasks;

namespace FinalTask_1_.Controllers
{
    public class HistoryController : Controller
    {

       
       IUserService service;
        IHistoryService service2;

        public HistoryController(IUserService  services,IHistoryService servises2)
        {
            service = services;
            service2 = servises2;
        }

        
        public ActionResult ChoiseHistory(string id1)
        {
            var alluser = service.GetAllUsers().ToList();
            var user = alluser.Find(x => x.Id == id1);
            ViewBag.user = user;
            ViewBag.friends = service.GetAllFriends(user);
            if (ViewBag.friends == null)
            {
                List<string> list = new List<string>();
                list.Add("У вас пока нет друзей");
                ViewBag.friends = list;
            }
            return View();
        }
        /// <summary>
        /// create new chat
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns></returns>

        public ActionResult NewChat(string id1,string id2)
        {
           
             var temp2 = service2.GetAll().Find(x => (x.receiverId == id2 && x.senderId == id1) || (x.receiverId == id1 && x.senderId == id2));
            if (temp2 == null)
            {
                var temp3 = service2.GetAll().Find(x => (x.receiverId == id2 && x.senderId == id1) || (x.receiverId == id1 && x.senderId == id2));
                return RedirectToAction("History", new { id1 = temp3.Id, id2 = id1 });
            }
            
            return RedirectToAction("History",new { id1=temp2.Id,id2=id1});
        }

        /// <summary>
        /// find chat
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns></returns>
        public ActionResult FindHistory(string id1, string id2)
        {
            var temp = service2.GetAll().Find(x => (x.receiverId == id2 && x.senderId == id1) || (x.receiverId == id1 && x.senderId == id2));//перенести в сервис
            if (temp == null)
            {

                HistoryModel model = new HistoryModel()
                {

                    senderId = id1,
                    receiverId = id2,
                    messagesIds = new List<int>()

                };
                service2.NewHistory(model);
                return RedirectToAction("NewChat", new { id1 = model.senderId, id2 = model.receiverId });
            }
            else
            {
              
                return RedirectToAction("History",new { id1=temp.Id,id2=id1});
            }
        }
        [HttpGet]
        public ActionResult History(int id1,string id2)

        {
          
            var history =  service2.FindHistory(id1);
            ViewBag.ourUser = service.Getwithoutdetails(id2).nick;
            ViewBag.id1 = history.senderId;
            ViewBag.id2 = history.receiverId;
            ViewBag.H = history.Id;
            ViewBag.messages = history.messagesIds;
            return View();
        }
       /// <summary>
       /// particial view for chat
       /// </summary>
       /// <param name="model"></param>
       /// <param name="id1"></param>
       /// <returns></returns>
        public async Task< ActionResult> History1(MessageModel model,int id1)
            ///флаг сюда тогда надо передавать id пользоватля который отправил сообщение
        {
           
            model.time = DateTime.Now;
            model.idHistory = id1;
           await service2.addMessage(model);
             var histori = service2.GetAllMessages(model.idHistory);
             
            return PartialView(histori);

          
        }
        /// <summary>
        /// particial view for update
        /// </summary>
        /// <param name="id1"></param>
        /// <returns></returns>
        public ActionResult Update(int id1)
        {
           ViewBag.histori = service2.GetAllMessages(id1);
            return PartialView();
        }
     
    }
}