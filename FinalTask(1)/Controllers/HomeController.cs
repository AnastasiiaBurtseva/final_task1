﻿using Business.Interfaces;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalTask_1_.Controllers
{
    public class HomeController : Controller
    {
        IUserService userservice;
        public HomeController(IUserService now)
        {

            userservice = now;
         
        }
        private IUserService UserService
        {
            get
            {
                return userservice;
            }
        }

       
        public ActionResult Index()
        {

            return View();
        }
          /// <summary>
          /// return client page
          /// </summary>
          /// <param name="id1"></param>
          /// <returns></returns>
        public ActionResult PersonalPage(string id1)
        {
            var alluser = userservice.GetAllUsers().ToList();
            var user = alluser.Find(x => x.Id == id1);
            ViewBag.user = user;
          
            ViewBag.friends = userservice.GetAllFriends(user);
          
            return View();
        }
    [Authorize(Roles = "admin")]
      
        public ActionResult About()
        {
            ViewBag.Message = "Функционал для администратора";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}