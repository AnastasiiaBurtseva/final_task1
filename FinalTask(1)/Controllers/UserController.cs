﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Interfaces;
using Business.Models;
using Business.Services;

namespace FinalTask_1_.Controllers
{ 
    public class UserController : Controller
    {
      
        
        IUserService service;
        public UserController(IUserService now)
        {
           service = now;

        }


       
        [HttpGet]
        public ActionResult Update(string id1)
        {
            if (id1 == null)
            {
                return HttpNotFound();
            }
            var alluser = service.GetAllUsers().ToList(); 
            
            var user = alluser.Find(x => x.Id==id1);
            if (user != null)
            {
                return View(user);
            }
            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult Update(UserModel user)
        {
            service.onlyUpdate(user);
            return RedirectToAction("Update");
        }
        /// <summary>
        /// searcher
        /// </summary>
        /// <param name="id1"></param>
        /// <returns></returns>
        public ActionResult FindFriend(string id1)
        {
            var alluser = service.GetAllUsers().ToList();
            ViewBag.users = alluser;
            ViewBag.ouruser = id1;
            return View();

        }
        /// <summary>
        /// searcher result
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id1"></param>
        /// <returns></returns>
        public ActionResult FriendsResult(UserModel model,string id1)
        {
            if (service.FindUser(model).Count != 0)
            {
                ViewBag.result = service.FindUser(model);
                ViewBag.ouruser = id1;
                return View();
            }

          
       
            ViewBag.result1 = "Ничего не найдено";

            ViewBag.ouruser = id1;
            return View();

        }
        public ActionResult AddFriend(string id1, string id2) //id1 хозяин  id2 друг
        {
            service.AddFriend(id1, id2);
           
            
            return View();
        }
    }
}