﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FinalTask_1_.Models
{
    public class LoginModel
    {
        [Required]
        public string nick { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
    }
}