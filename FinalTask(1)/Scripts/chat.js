﻿

        $(document).ready(function() {
            ChatOnSuccess();
        });

//при успешном входе загружаем сообщения
function LoginOnSuccess(result) {

    Scroll();
    ShowLastRefresh();

    //каждые пять секунд обновляем чат
    setTimeout("Refresh();", 1000);

    //отправка сообщений по нажатию Enter
    $('#txtMessage').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $("#btnMessage").click();
        }
    });

}

    //при ошибке отображаем сообщение об ошибке при логине
    function LoginOnFailure(result) {
        $("#Username").val("");
        $("#Error").text(result.responseText);
        setTimeout("$('#Error').empty();", 1000);
    }

    // каждые пять секунд обновляем поле чата
function Refresh() {
 
    ShowLastRefresh();
    var href = "/History/Update/" + idroom;

    $("#ActionLink").attr("href", href)[0].click();
 
    
   
        }


    //Отображаем сообщение об ошибке
    function ChatOnFailure(result) {
        $("#Error").text(result.responseText);
        setTimeout("$('#Error').empty();", 3000);
    }

    // при успешном получении ответа с сервера
    function ChatOnSuccess(result) {
       
       
        setInterval("Refresh();", 3000);
    }

    //скролл к низу окна
    function Scroll() {
        var win = $('#Messages');
        var height = win[0].scrollHeight;
        win.scrollTop(height);
    }

    //отображение времени последнего обновления чата
    function ShowLastRefresh() {
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $("#LastRefresh").text("Последнее обновление было в " + time);
    }
	
