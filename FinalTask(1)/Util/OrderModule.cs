﻿using Business.Interfaces;
using Business.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalTask_1_.Util
{
    public class OrderModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserServices>();
            Bind<IRoleService>().To<RoleService>();
            Bind<IHistoryService>().To<HistoryServise>();
            Bind<IMessageService>().To<MessageService>();

        }

    }
}